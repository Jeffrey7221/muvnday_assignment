import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import User from './User';
import Tasks from './Tasks/Tasks';
import displayTasks from './Tasks/displayTasks';
import { BrowserRouter as Router, Route } from "react-router-dom";
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <Router>
        <div className= "container">
            <Route exact path="/" component = { App } />
            <Route path="/User" component = { User } />
            <Route path="/Tasks" component = { Tasks } />
            <Route path="/displayTasks" component = { displayTasks } />
        </div>
    </Router>,
    document.getElementById('root')
);
// ReactDOM.render(<User />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
