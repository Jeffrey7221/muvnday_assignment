import React from 'react';
import {withRouter, Redirect} from 'react-router';
import firebase from './Firestore';
// import axios from 'axios';
// import './User.css';

class User extends React.Component {
    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            duplicate: false,
            created: false,
        };
    }

    updateInput = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    addUser = e => {
        e.preventDefault();
        const db = firebase.firestore();
        const users_db =  db.collection("users");
        const duplicate_check = users_db.where("email", "==", this.state.email);
        var self = this;
        duplicate_check.get().then(function(user) {
            if(user.docs.length !== 0) {
                console.log(user.docs.length);
                self.handle_duplicate();
            } else {
                const userRef = users_db.add({
                    email: self.state.email,
                    password: self.state.password
                });
                self.setState({
                    email: "",
                    password: "",
                    duplicate: false,
                    created: true
                });
            }
        }).catch(function(error) {
            console.log("Error: ", error);
        });
        // this.fetchUser();
    };

    handle_duplicate = e => {
        this.setState({
            duplicate: true,
        });
        var element = document.querySelector("#register");
        element.style.display = "inline-block";
        console.log("handle");
    }    

    render() {
        if(this.state.created === true) {
            return <Redirect to='/' />
        }

        return(
            <div className="App">
                <header className="App-header">
                    <p>
                        Register new user
                    </p>
                    <p className="warning" id="register">
                        An account already exists with this email!
                    </p>
                    <form onSubmit={this.addUser.bind(this)}>
                        <p>
                            <input 
                                type="email"
                                name="email"
                                className="email"
                                placeholder="email"
                                onChange={this.updateInput}
                                value={this.state.email}
                            />
                        </p>
                        <p>
                            <input
                                type="password"
                                name="password"
                                className="password"
                                placeholder="Password"
                                onChange={this.updateInput}
                                value={this.state.password}
                            />
                        </p>
                        <p>
                            <button type="submit">Sign up</button>
                        </p>
                    </form>
                </header>
            </div>
        );
    }
}

export default withRouter(User);
