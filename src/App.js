import React, { Component } from 'react';
import logo from './logo.svg';
import firebase from './Firestore';
import { Link, Redirect } from "react-router-dom";
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
        email: "",
        password: "",
        logged_in: false,
    };
  }

  updateInput = e => {
    this.setState({
        [e.target.name]: e.target.value
    });
  }

  checkInput = e => {
    e.preventDefault();
    const db = firebase.firestore();
    const users_db =  db.collection("users");
    const duplicate_check = users_db.where("email", "==", this.state.email);
    var self = this;
    duplicate_check.get().then(function(user) {
        if(user.docs.length !== 0) {
            console.log(user.docs.length);
            self.setState({
              logged_in: true,
            })
        } else {
            self.invalid_login();
        }
    }).catch(function(error) {
        console.log("Error: ", error);
    });
  };

  invalid_login = e => {
    this.setState({
      duplicate: true,
    });
    var element = document.querySelector("#login");
    element.style.display = "inline-block";
    console.log("handle");
  }    

  render() {
    if(this.state.logged_in === true) {
      localStorage.setItem('email', this.state.email);
      return <Redirect to='./Tasks' />
    }

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p className="warning" id="login">
            Invalid Login
          </p>
          <form onSubmit={this.checkInput}>
            <p>
              <input 
                type="email"
                name="email"
                className="email"
                placeholder="email"
                onChange={this.updateInput}
                value={this.state.email}
              />
              <input
                  type="password"
                  name="password"
                  className="password"
                  placeholder="password"
                  onChange={this.updateInput}
                  value={this.state.password}
              />
              <button type="submit">Submit</button>
            </p>
          </form>
            <Link id="link" to='./User'>
              <p id='users_page'>New Users, click here</p>
            </Link>
        </header>
      </div>
    );
  }
}

export default App;
