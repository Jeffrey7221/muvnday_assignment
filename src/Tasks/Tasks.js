import React from 'react';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import firebase from '../Firestore';
import './Tasks.css';

class Tasks extends React.Component {
    constructor() {
        super();
        this.state = {
            email: localStorage.getItem('email'),
            title: "",
            description: "",
            date: "",
            duplicate: false
        }
      }

    updateInput = e => {
    this.setState({
        [e.target.name]: e.target.value
    });
    }

    addTask = e => {
        e.preventDefault();
        var element = document.querySelector("#exists");
        element.style.display = "none";
        element = document.querySelector("#incomplete");
        element.style.display = "none";
        element = document.querySelector("#submitted");
        element.style.display = "none";
        if(this.state.title === "" || this.state.description === "" || this.state.date === "") {
            this.incomplete_form();
            return;
        } else {
            const db = firebase.firestore();
            const users_db =  db.collection("tasks");
            const user_db = users_db.where("email", "==", this.state.email);
            const duplicate_check = user_db.where("title", "==", this.state.title);
            var self = this;
            duplicate_check.get().then(function(user) {
                if(user.docs.length !== 0) {
                    console.log(user.docs.length);
                    self.handle_duplicate();
                } else {
                    const userRef = users_db.add({
                        email: self.state.email,
                        title: self.state.title,
                        description: self.state.description,
                        date: self.state.date,
                    });
                    self.setState({
                        duplicate: false,
                        created: true
                    });
                    var element = document.querySelector("#submitted");
                    element.style.display = "inline-block";
                }
            }).catch(function(error) {
                console.log("Error: ", error);
            });
        }
    };

    incomplete_form = e => {
        var element = document.querySelector("#incomplete");
        element.style.display = "inline-block";
    }

    handle_duplicate = e => {
        this.setState({
            duplicate: true,
        });
        var element = document.querySelector("#exists");
        element.style.display = "inline-block";
    }

    render() {
        return(
            <div className="App">
                <header className="App-header">
                        <Link id="viewButton" to='./displayTasks'>
                            <p id='users_page'>Click here to view tasks</p>
                        </Link>
                    <p className="warning" id="exists">Task already exists with given name</p>
                    <p className="warning" id="incomplete">Please complete all fields before submitting</p>
                    <p className="warning" id="submitted">Task successfully submitted</p>
                    {/* <div id="form_body"> */}
                        <form id="task_form" onSubmit={this.addTask}>
                            <label className="subfield">
                                <span className="field_title">Title:</span>
                                <input 
                                    type="text" 
                                    name="title" 
                                    className="input"
                                    onChange={this.updateInput}
                                    value={this.state.title}
                                />
                            </label> 
                            <label className="subfield">
                                <span className="field_title">Description:</span>                                
                                <textarea 
                                    name="description" 
                                    className="input"
                                    rows={5}
                                    onChange={this.updateInput}
                                    value={this.state.description}
                                />
                                
                            </label>
                            <label className='subfield'>
                                <span className="field_Title">Select Date</span>
                                <input 
                                    name="date"
                                    type="Date"
                                    className="input"
                                    onChange={this.updateInput}
                                    value={this.state.date}
                                />
                            </label >
                            <button type="submit">Sign up</button>
                        </form>
                    {/* </div> */}
                </header>
                
            </div>
        );
    }
}

export default withRouter(Tasks);
