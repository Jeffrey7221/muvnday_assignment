import React from 'react';
import {withRouter } from 'react-router';
import firebase from '../Firestore';
import { Link } from 'react-router-dom';

class Tasks extends React.Component {
    constructor() {
        super();
        this.state = {
            email: localStorage.getItem('email'),
            title: "",
            description: "",
            date: "",
            duplicate: false,
            list: [],
        }
        this.update = null;
        this.user_db = firebase.firestore().collection("tasks").where("email", "==", this.state.email);


    }

    onCollectionUpdate = (querySnapshot) => {
        const tasks = [];
        querySnapshot.forEach((doc) => {
            const { title, description, date } = doc.data();
            tasks.push({
                key: title,
                doc,
                title,
                description,
                date,
            });
        });
        this.setState({
            list: tasks,
        });
    }

    componentDidMount() {
        this.update = this.user_db.onSnapshot(this.onCollectionUpdate);
    }

    render() {
        return(
            <div className="App">
                <header className="App-header">
                        <Link id="viewButton" to='./Tasks'>
                            <p id='users_page'>Click here to create tasks</p>
                        </Link>
                    <table className="taskTable">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.list.map(item =>
                                <tr>
                                    <td>{item.title}</td>
                                    <td>{item.description}</td>
                                    <td>{item.date}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </header>
            </div>
        );
    }
}

export default withRouter(Tasks);
